
# Aardvark opinionated process

To be able to make Aardvark easy to use and general for many projects
it is highly opinionated on parts of the process. This document
describes the intended process to use when developing a project
using Aardvark.

Each project consist of two repositories on GitLab. We will call it
`MY-PROJECT` and `MY-PROJECT-infra`. The first repository contains
the source code and
build instructions using a `Dockerfile`, see
[build usage document](usage-build.md) for details. The second
project must be named the same as the first with the suffix
`-infra`. It contains
the Kubernetes manifest for deployment in the form of
[Kustomize](https://kustomize.io/) files.
See [deploy usage document](usage-deploy.md) for details.

Both repositories must have `main` or `master` as default branch.

The following image illustrates the working process. Read along to
get the details.
[![Image describing process](images/version-controll-process.jpg)](images/version-controll-process.jpg)

The process has four stages:

* Developer works on a personal branch and develop their feature etc.
  Every time the branch is pushed to GitLab, an image is build.
  The image is tagged with the commit sha and branch name as tags. This makes it easy for the developer to evaluate the build.
  No automatic deployment happens.
* Common development on `main`or `master` branch. Before merge the
  personal branch, it is recommended it is peer-reviewed.
  Once the feature is merged to the default branch a new image is build with commit sha and the tag *main* or *master* (depending on default branch name).
  Once build, a change is automatically done in the infra project and
  the new build is installed in the development environment.
* Release to test environment. Release manager tag the commit to be
  released with `test`. An installation of the commit is done to the
  test environnement. Observe that it is not necessary to do a new
  image build, since the commit has already been build earlier.
  It is not a requirement that the tag is set on the default branch,
  any commit in any branch works. Though, it is recommended to use the
  default branch for this.
* Release to production environment. The release manager tag a
  commit with `prod`. Ideally this is the same commit as currently
  used in test. The tag triggers an install of the image for the commit
  to the production environment.
  Observe that it is not necessary to do a new
  image build, since the commit has already been build earlier. This
  guarantees nothing have changed between test and production. It is not
  necessary to set the tag on the default branch, any branch works.
  Though, it is recommended to use the default branch for this.

These steps are illustrated in [this presentation](https://docs.google.com/presentation/d/e/2PACX-1vRqEmkly2UA0oxrctfKYCaykAJpgscN2J7RPbhAs_rt1VX3PMZe8MM9jmvznqQ3sQRn57X8zR_p521d/pub?start=true&loop=false&delayms=3000).
More details of the steps is described below.

## Build and publish an image for my personal branch

For the repository `MY-PROJECT` is every push building a new image, no mather what branch you work on. This image can be used by the developer to do local testing etc. The image is named `docker-images.jobtechdev.se/MY-PROJECT/MY-PROJECT` and get two tags. One tag is the first seven characters of the git commit id. The other tag is the branch name.

**Note:** Avoid to name branches `develop`, `test`, `staging`, `prod`, or `prod-stdby`. These names are reserved for the release process.

## Deploy a development release to common environment

When one or several commits are merged into the default
branch(`master`/`main`) a new image is build. This image also get two
tags. It is tagged with the same pattern as in the personal branch.
First tag is the git commit id, second is `main` or `master`
depending on your default branch.

New commits to the default branch are released to a common development
environment. The release is done during the build process. The build
pipeline update the git repository `MY-PROJECT-infra` changing the
file `kustomize/overlays/develop/kustomization.yaml`.
It change the image tag to be the newly build image, using the sha tag.
This change starts a deployment to the development environment.

## Release a test release

Once the default branch in your repository `MY-PROJECT` is in a state
where you want to release it to a test environment you enter this phase.
The release manager tags the git-commit you want to release with
the tag `test` and pushes it to GitLab.
Details on how to do the tagging is described in a
[separate document](tag-for-release.md).
When the tag is pushed to GitLab, Aardvark updates `MY-PROJECT-infra`
repository and the file `kustomize/overlays/staging/kustomization.yaml`
with the new commit sha to refer correct image. This change starts a
deployment to the test environment.

**Note:** You can only tag commits for release where the image is already build. Otherwise the deployment process will not find any image. No new image is build when doing the release.

Some projects prefer use the term *staging*, then use the tag `staging` instead and name your Kustomize overlay `staging`.

## Release a production release

Releasing to production works the same way as release to testing. The
release-manager tag the git-commit to be released with `prod`. If you
have a separate standby environment for production, releases to it can
be controlled separately using the git tag `prod-stdby`.

You can set any tag on a commit other than `test`, `staging`, `i1`, `t2`, `prod` or
`prod-stdby`. The container image will get that tag too. No update to
`MY-PROJECT-infra` will happen for these tags. This is useful to keep
track on release history.

**Note:** The release of production do *no* verification that you have
tested your image. Good practice is to tag the commit currently in
test. Then you are guaranteed to run same code in production as in test.

**Note:** The release of test or production do *not* require the release is done on the default branch. Any git-commit can be tagged for release. This enables easy patching if needed.

# Patching to production

Assume you have merged a couple of new features to the default branch but they are still not well tested.
A new bug is discovered in production that is urgent to fix. Easiest way to release the fix is to:

1. Fix the bug in a branch and push the branch to GitLab.
1. Tag the branch last commit with `test`, and it will install in the test environment.
1. Verify the bug is fixed.
1. Tag the branch last commit with `prod`, and it will install into production.
1. Merge your branch to default branch so the bug does not reoccur in next release.

# Remote clusters

It is not always one want to install releases on the same cluster as
the software is build. To not require GitLab to have access to the
cluster deploying, [ArgoCD](https://argo-cd.readthedocs.io/en/stable/)
can be used. ArgoCD is pulling the infra git-repository and ensures it
is up to date.
Reason not to use this method for deploying the head of the default
branch is it introduces an extra delay and we want developer iterations to
be swift.
More on remote deployment can be found [here](remote-deploy.md).
