# This pipeline deploys current development version.
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: aardvark-infra-check-pipeline
spec:
  workspaces:
  - name: shared-workspace
  params:
  - name: git-url
    type: string
    description: url of the git repo for the code
  - name: gitlab-repository-full-name
    type: string
    description: |
      The GitLab repository full name, e.g.: arbetsformedlingen/devops/aardvark-demo
  - name: git-commit
    description: sha of commit to deploy
  - name: gitlab-project
    description: ID of GitLab project.
  - name: cluster-name
    description: Name of cluster pipeline run in
    default: ""
  - name: ocs-ui-base-url
    type: string
    description: Base URL to OCS admin interface.
    default: "https://console-openshift-console.test.services.jtech.se"
  tasks:
  - name: gitlab-run-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-infra-check"
    - name: STATE
      value: running
    - name: DESCRIPTION
      value: Check of infra started.
  - name: fetch-repository
    taskRef:
      name: git-clone
      kind: ClusterTask
    workspaces:
    - name: output
      workspace: shared-workspace
    params:
    - name: url
      value: $(params.git-url)
    - name: subdirectory
      value: "source"
    - name: deleteExisting
      value: "true"
    - name: revision
      value: $(params.git-commit)
    - name: submodules
      value: "false"
    runAfter:
    - gitlab-run-status
  - name: generate
    taskRef:
      name: openshift-client
      kind: Task
    workspaces:
    - name: source
      workspace: shared-workspace
    params:
    - name: SCRIPT
      value: |
        if [ -d kustomize ]
        then
          ok="true"
          mkdir -p $(workspaces.source.path)/generated
          for i in kustomize/overlays/*; do
            e=$(echo $i | cut -d/ -f3)
            if ! oc kustomize $i > $(workspaces.source.path)/generated/$e.yaml ; then
              ok="false"
              echo Environment $e cannot be generated.
              oc kustomize $i
              echo ======================================
              echo
            fi
          done
          if [ ${ok} = "false" ] ; then
            exit 1
          fi
        fi
    runAfter:
    - fetch-repository
  - name: lint
    taskRef:
      name: kube-linter
      kind: Task
    workspaces:
    - name: source
      workspace: shared-workspace
    params:
    - name: config_map
      value: kubelinter
    - name: manifest
      value: $(workspaces.source.path)/generated
    - name: output_format
      value: plain
    - name: output_file
      value: $(workspaces.source.path)/generated/kubelint.txt
    runAfter:
    - generate
  - name: post-lint-review
    taskRef:
      name: post-review-comment
      kind: Task
    workspaces:
    - name: source
      workspace: shared-workspace
    params:
    - name: review-file
      value: generated/kubelint.txt
    - name: commit-sha
      value: "$(params.git-commit)"
    - name: project
      value: "$(params.gitlab-project)"
    runAfter:
    - lint
  finally:
  - name: gitlab-success-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Succeeded"
      - "Completed"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-infra-check"
    - name: STATE
      value: success
    - name: DESCRIPTION
      value: Check of infrastructure code passed.
  - name: gitlab-failed-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Failed"
      - "None"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-infra-check"
    - name: STATE
      value: failed
    - name: DESCRIPTION
      value: Check of infrastructure code passed. See logs for pipeline.
