# This pipeline deploys current development version.
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: aardvark-deployer-pipeline
spec:
  workspaces:
  - name: shared-workspace
  params:
  - name: git-url
    type: string
    description: url of the git repo for the code
  - name: gitlab-repository-full-name
    type: string
    description: |
      The GitLab repository full name, e.g.: arbetsformedlingen/devops/aardvark-demo
  - name: git-commit
    description: sha of commit to deploy
  - name: cluster-name
    description: Name of cluster pipeline run in
    default: ""
  - name: ocs-ui-base-url
    type: string
    description: Base URL to OCS admin interface.
    default: "https://console-openshift-console.test.services.jtech.se"
  tasks:
  - name: gitlab-run-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-deploy"
    - name: STATE
      value: running
    - name: DESCRIPTION
      value: Build has been trigged and is building.
  - name: fetch-repository
    taskRef:
      name: git-clone
      kind: ClusterTask
    workspaces:
    - name: output
      workspace: shared-workspace
    params:
    - name: url
      value: $(params.git-url)
    - name: subdirectory
      value: "source"
    - name: deleteExisting
      value: "true"
    - name: revision
      value: $(params.git-commit)
    - name: submodules
      value: "false"
    runAfter:
    - gitlab-run-status
  - name: deploy
    taskRef:
      name: openshift-client
      kind: Task
    workspaces:
    - name: source
      workspace: shared-workspace
    params:
    - name: SCRIPT
      value: |
        if [ -d kustomize ]
        then
          for dir in $(ls -d kustomize/overlays/*)
          do
            if grep '#cluster:' ${dir}/kustomization.yaml; then
              DEST=$(grep '#cluster:' ${dir}/kustomization.yaml | cut -d ':' -f 2 | sed -e 's/^[[:space:]]*//')
              if [ $DEST = $(params.cluster-name) ]
              then
                echo Deploying overlay $dir.
                oc apply -k $dir
                if [ $? == 0 ]
                then
                  echo Done deploying $dir
                else
                  echo Failed deploying $dir
                fi
              fi
            fi
          done
        fi
    runAfter:
    - fetch-repository
  finally:
  - name: gitlab-success-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Succeeded"
      - "Completed"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-deploy"
    - name: STATE
      value: success
    - name: DESCRIPTION
      value: Build has been finalized with success.
  - name: gitlab-failed-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Failed"
      - "None"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/aardvark-deploy"
    - name: STATE
      value: failed
    - name: DESCRIPTION
      value: Build failed. Check logs for details.
